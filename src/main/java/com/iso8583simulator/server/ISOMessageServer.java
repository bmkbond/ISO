package com.iso8583simulator.server;
package com..iso8583simulator.server;

import java.net.InetSocketAddress;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com..iso8583simulator.config.NettyProperties;
import com..iso8583simulator.handler.ISOMessageServerInitializer;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;




@Service("isoMessagesServer")
public class ISOMessageServer {
	
	@Autowired
	private NettyProperties nettyProperties;
	
	EventLoopGroup bossGroup = new NioEventLoopGroup(1);
    EventLoopGroup workerGroup = new NioEventLoopGroup();

    public void start() {

        try {
            ServerBootstrap bootStrap = new ServerBootstrap();
            bootStrap.group(bossGroup, workerGroup).channel(NioServerSocketChannel.class)
                    .handler(new LoggingHandler(LogLevel.INFO)).childHandler(new ISOMessageServerInitializer());

            ChannelFuture channelFuture = bootStrap.bind(new InetSocketAddress(nettyProperties.getTcpPort()))
                    .sync();
            System.out.println("ISO Messages Server started on port " + nettyProperties.getTcpPort());
            channelFuture.channel().closeFuture().sync();
        } catch (InterruptedException e) {
            //
        } finally {
            workerGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();

        }
        System.exit(0);
    }

    public void restart() {
    	bossGroup.shutdownGracefully();
        this.start();
    }

    public void stop() {
    	bossGroup.shutdownGracefully();
    }
}
