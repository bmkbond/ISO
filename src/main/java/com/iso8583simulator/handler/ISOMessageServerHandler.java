package com.iso8583simulator.handler;

package com..iso8583simulator.handler;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

public class ISOMessageServerHandler extends SimpleChannelInboundHandler<ISOMsg> {

	private static final Logger log = LoggerFactory.getLogger(ISOMessageServerHandler.class);

	@Override
	public void channelRead0(ChannelHandlerContext ctx, ISOMsg requestMessage) throws Exception {
		ISOMsg responseMessage = prepareAuthResponse(requestMessage);
		ctx.writeAndFlush(responseMessage);
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
		cause.printStackTrace();
		log.error("Exception Caught {}", cause.toString());
		ctx.close();
	}

	private ISOMsg prepareAuthResponse(ISOMsg message) {
		ISOMsg responseISOMsg = new ISOMsg();

		try {
			responseISOMsg.setMTI("0110");
			responseISOMsg.set(2, (String) message.getValue(2));
			responseISOMsg.set(3, (String) message.getValue(3));
			responseISOMsg.set(4, (String) message.getValue(4));
			responseISOMsg.set(7, (String) message.getValue(7));
			responseISOMsg.set(11, (String) message.getValue(11));
			responseISOMsg.set(14, (String) message.getValue(14));
			responseISOMsg.set(22, (String) message.getValue(22));
			responseISOMsg.set(37, (String) message.getValue(37));
			responseISOMsg.set(38, "010305");
			responseISOMsg.set(39, "00");
			responseISOMsg.set(42, (String) message.getValue(42));
			responseISOMsg.set(49, (String) message.getValue(49));

		} catch (ISOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return responseISOMsg;
	}

}
