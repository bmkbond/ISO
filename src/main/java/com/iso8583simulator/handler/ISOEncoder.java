package com.iso8583simulator.handler;
package com..iso8583simulator.handler;

import java.io.ByteArrayOutputStream;

import org.apache.commons.codec.binary.Hex;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.packager.GenericPackager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;


import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

public class ISOEncoder extends MessageToByteEncoder<ISOMsg> {

    private static final Logger log = LoggerFactory.getLogger(ISOEncoder.class);

    private static final int DEFAULT_BUFFER_LENGTH = 256;

  
    public void encode(ChannelHandlerContext ctx, ISOMsg msg, ByteBuf out) throws Exception {
        final ByteArrayOutputStream os = new ByteArrayOutputStream(DEFAULT_BUFFER_LENGTH);
        try {
        	GenericPackager packager = new GenericPackager("basic.xml");
          //  log.debug(MessageUtils.describeMessage(msg));
          //  messageFactory = SpringWrapper.getBean("DCIDATA", MessageFactory.class);
          //  messageFactory.writeToStream(msg, os);
           // String hexReq = HexUtils.toHexString(os.toByteArray());
           // int length = os.size();
           // StringBuilder sb = new StringBuilder();
           // sb.append(HexUtils.toHexString(getStrNumber(4, length)));
            //sb.append(hexReq);
           // sb.append(DciConstants.ETX);
           // byte[] req = getByteArray(sb.toString());
        	msg.setPackager(packager);
        	byte[] data = msg.pack();
            out.writeBytes(data);
            log.debug("Sending response message to DCI host : {} ", Hex.encodeHexString(data));
        } catch (Exception e) {
            log.error("Unable to encode data: " + e);
            throw new IllegalStateException("unable to encode data, " + e.getMessage());
        }

    }

    public static String getStrNumber(int length, int num) {
        String s = "" + num;
        if (s.length() > length) {
            throw new RuntimeException("Number length is more than the expected..");
        }
        for (int i = s.length(); i < length; i++) {
            s = "0" + s;
        }
        return s;
    }

    public static byte[] getByteArray(String hexString) {
        if (hexString == null) {
            return null;
        }

        int len = hexString.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(hexString.charAt(i), 16) << 4)
                    + Character.digit(hexString.charAt(i + 1), 16));
        }
        return data;
    }
}
