package com.iso8583simulator.handler;
package com..iso8583simulator.handler;

import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.Security;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;

import org.springframework.beans.factory.annotation.Value;

 

public final class MessageHostContextFactory { 
 
    private static final String PROTOCOL = "TLSv1.2"; 
    private static final SSLContext SERVER_CONTEXT; 
    private static final SSLContext CLIENT_CONTEXT; 
    
    @Value("${keystorePath}")
	private static String keystorePath;
    
    @Value("${keystorePassword}")
	private static String keystorePassword;
 
    static { 
        String algorithm = Security.getProperty("ssl.KeyManagerFactory.algorithm"); 
        if (algorithm == null) { 
            algorithm = "SunX509"; 
        } 
 
        SSLContext serverContext = null; 
        SSLContext clientContext = null; 
        try { 
        	String sslCertificatePath= "D:/certificate/cacerts.jks";
        	String sslPassword="changeit";
            KeyStore ks = KeyStore.getInstance("JKS"); 
            ks.load(new FileInputStream(sslCertificatePath),sslPassword.toCharArray()); 
 
            // Set up key manager factory to use our key store 
            KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory
                    .getDefaultAlgorithm()); 
            kmf.init(ks, sslPassword.toCharArray()); 
 
            // Initialize the SSLContext to work with our key managers. 
            serverContext = SSLContext.getInstance(PROTOCOL); 
            serverContext.init(kmf.getKeyManagers(), null, null); 
        } catch (Exception e) { 
            throw new Error( 
                    "Failed to initialize the server-side SSLContext", e); 
        } 
 
        try { 
            clientContext = SSLContext.getInstance(PROTOCOL); 
            clientContext.init(null, MessageHostTrustManagerFactory.getTrustManagers(), null); 
        } catch (Exception e) { 
            throw new Error( 
                    "Failed to initialize the client-side SSLContext", e); 
        } 
 
        SERVER_CONTEXT = serverContext; 
        CLIENT_CONTEXT = clientContext; 
    } 
 
    public static SSLContext getServerContext() { 
        return SERVER_CONTEXT; 
    } 
 
    public static SSLContext getClientContext() { 
        return CLIENT_CONTEXT; 
    } 
 
    private MessageHostContextFactory() { 
        // Unused 
    } 
}
