package com.iso8583simulator.handler;
package com..iso8583simulator.handler;

import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.packager.GenericPackager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.bytes.ByteArrayDecoder;

public class ISODecoder extends ByteArrayDecoder {

    private static final Logger log = LoggerFactory.getLogger(ISODecoder.class);

  //  private MessageFactory messageFactory;

    @Override
    public void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {

    	GenericPackager packager = new GenericPackager("basic.xml");
        if (in.isReadable()) {
            //byte[] isoMessageLength = new byte[in.readableBytes()];
           // in.getBytes(0, isoMessageLength);
            //byte[] header = new byte[2];
            byte[] message = new byte[in.readableBytes()];
            //in.getBytes(0, header);
            in.getBytes(0, message);
            log.debug("Request Recieved: {} ", toHexString(message));
            //in.getBytes(4, message);
            ISOMsg isoMsg = new ISOMsg();
            isoMsg.setPackager(packager);
            isoMsg.unpack(message);
            in.clear();
            logISOMsg(isoMsg);
            out.add(isoMsg);
        }

    }

    public static String toHexString(String str) {
        byte[] b = str.getBytes();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < b.length; i++) {
            sb.append(Integer.toHexString((b[i] & 0xF0) >> 4).toUpperCase());
            sb.append(Integer.toHexString(b[i] & 0x0F).toUpperCase());
        }
        return sb.toString();
    }
    
    public static void logISOMsg(ISOMsg msg) {
		System.out.println("----ISO MESSAGE-----");
		try {
			System.out.println("  MTI : " + msg.getMTI());
			for (int i=1;i<=msg.getMaxField();i++) {
				if (msg.hasField(i)) {
					System.out.println("    Field-"+i+" : "+msg.getString(i));
				}
			}
		} catch (ISOException e) {
			e.printStackTrace();
		} finally {
			System.out.println("--------------------");
		}
 
	}
    public static String toHexString(byte[] array) {
        return DatatypeConverter.printHexBinary(array);
    }
}
