package com.iso8583simulator.handler;
package com..iso8583simulator.handler;

import javax.net.ssl.SSLEngine;

import org.springframework.stereotype.Component;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.ssl.SslHandler;

/**
 * Creates a newly configured {@link ChannelPipeline} for a server-side channel.
 */
@Component
public class ISOMessageServerInitializer extends ChannelInitializer<SocketChannel> {

	@Override
    public void initChannel(SocketChannel socketChannel) throws Exception {
        ChannelPipeline channelPipeline = socketChannel.pipeline();
        if (Boolean.parseBoolean("false")) {
            SSLEngine engine = MessageHostContextFactory.getServerContext().createSSLEngine();
                engine.setUseClientMode(false);
                channelPipeline.addLast("ssl", new SslHandler(engine)); 
        }    
        channelPipeline.addLast(new ISODecoder());
        channelPipeline.addLast(new ISOEncoder());        
        channelPipeline.addLast(new ISOMessageServerHandler());
      
    }

}
