package com.iso8583simulator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import com..iso8583simulator.server.ISOMessageServer;

@SpringBootApplication
public class ISOSimulatorApplication {

	
	
	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(ISOSimulatorApplication.class, args);
		ISOMessageServer isoMessagesServer = context.getBean(ISOMessageServer.class);
		isoMessagesServer.start();
	}
}
